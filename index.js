const puppeteer = require('puppeteer');

const [_node, _script, url, exportName] = process.argv;

console.log('URL: ', url);
console.log('Export name: ', exportName);

(async () => {
  const browser = await puppeteer.launch({ args: ['--no-sandbox'] });
  const page = await browser.newPage();
  await page.goto(url, { waitUntil: 'networkidle' })
  await page.pdf({
    path: `/exports/${exportName}.pdf`, format: 'A4',
    printBackground: true
  });

  await browser.close();
})();